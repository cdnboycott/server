package main

import (
	_ "grocerycompare_server/docs"
	_ "grocerycompare_server/model"
)

// @title grocerycompare
// @version 0.0.2
// @description grocerycompare

// @host localhost
// @securityDefinitions.basic BasicAuth
// @BasePath /v1
func main() {
	r := &Router{}
	r.New()
}
