package model

type Tag struct {
	Id   int `json:"-"`
	Name string
}
